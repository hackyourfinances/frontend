const BACKEND_URL = 'http://localhost:5000/'

class FinanceApi {
  constructor () {
    this.userData = []
    this.userID = 1
  }

  fetchUserData () {
    const url = 'user/' + this.userID
    return this._makeRequest(url, {
      method: 'GET'
    }).then(userData => {
      this.userData = userData
      this._handleChange()
    }).catch(error => console.error('failed to get userData', error))
  }

  getUserData () {
    return this.userData
  }

  putAsset (category, monthlyIncome) {
    const url = 'assets'
    return this._makeRequest(url, {
      method: 'PUT',
      body: {
        user_id: this.userID,
        category: category,
        asset: monthlyIncome }
    }).then(() => {
      return this.fetchUserData()
    })
  }

  putScore (courseID, courseName, courseScore) {
    const url = 'score'
    return this._makeRequest(url, {
      method: 'PUT',
      body: {
        user_id: this.userID,
        course_id: courseID,
        course_name: courseName,
        course_score: courseScore }
    }).then(() => {
      return this.fetchUserData()
    })
  }

  postGoal (goalName, goalDesc, goalTarget) {
    const url = 'goals'
    return this._makeRequest(url, {
      method: 'POST',
      body: {
        user_id: this.userID,
        goal_name: goalName,
        goal_desc: goalDesc,
        goal_target: goalTarget }
    }).then(() => {
      return this.fetchUserData()
    })
  }

  _handleChange () {
    if (this.onChange !== null) this.onChange()
  }

  subscribe (onChange) {
    this.onChange = onChange
  }

  _makeRequest (url, options) {
    options.headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }

    if (options.body) {
      options.body = JSON.stringify(options.body)
    }

    return fetch(BACKEND_URL + url, options).then((response) => {
      if (response.ok) {
        return response.json()
      } else {
        return response.json().then(({ errors }) => {
          const error = new Error(response.statusText)
          response.errorMessages = errors
          throw error
        })
      }
    })
  }
}

export default FinanceApi
