import React from 'react'
import { Switch, Route, BrowserRouter } from 'react-router-dom'
import { Header, Container } from 'semantic-ui-react'
import HomePage from './pages/home'
import Learn from './pages/learn'
import Goals from './pages/goals'
import Budget from './pages/budget'
import Navigation from './components/Navigation'
import 'react-sweet-progress/lib/style.css'
import Upcoming from './pages/upcoming'

const App = (props) => (
  <div>
    <BrowserRouter>
      <Navigation service={props.service} userData={props.service.getUserData()}/>
      <Container textAlign='center'>
        <Header as='h1'>Hack Your Finances</Header>
      </Container>
      <Switch>
        <Route path='/goals' exact component={() => <Goals userData={props.service.getUserData()} />} />
        <Route path='/upcoming' exact component={() => <Upcoming userData={props.service.getUserData()} />} />
        <Route path='/budget' exact component={() => <Budget userData={props.service.getUserData()} />} />
        <Route path='/learn' exact component={() => <Learn userData={props.service.getUserData} />} />
        <Route path='/' exact component={() => <HomePage service={props.service} />} />
      </Switch>

    </BrowserRouter>
  </div>
)

export default App
