import React, { Component } from 'react'
import { Container, Card, Header } from 'semantic-ui-react'
import { testGoals } from './goals_test_data'

class Goals extends Component {
  constructor (props) {
    super(props)

    this.state = {
      userData: this.props.userData
    }
  }

  render () {
    if (typeof this.state.userData !== 'undefined') {
      let money = '$' + this.state.userData.goal_target
      let name = '' + this.state.userData.goal_name
      let desc = '' + this.state.userData.goal_desc
      return (
        <Container style={{ margin: '40px' }}>
          <Card fluid color='red' header={name} meta={money} description={desc} />
        </Container>
      )
    } else {
      return (
        <Container textAlign='center' style={{ marginTop: '40px' }}>
          <Header as='h1'> YOU HAVE NO GOALS </Header>
        </Container>
      )
    }
  }
}

export default Goals
