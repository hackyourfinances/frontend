import React, { Component } from 'react'
import { Table } from 'semantic-ui-react'
import PieChart from 'react-minimal-pie-chart'
import { testGoals } from './goals_test_data'
import { Container, Card, Header } from 'semantic-ui-react'

class Budget extends Component {
  constructor (props) {
    super(props)

    this.state = {
      userData: this.props.userData
    }
  }

  render () {
    if (typeof this.state.userData !== 'undefined') {
      let money = '$' + this.state.userData.goal_target
      let name = '' + this.state.userData.goal_name
      let desc = '' + this.state.userData.goal_desc

      // set budget below from goals_test_data

      let housingBudget = testGoals['budget']['housing']
      let utilitiesBudget = testGoals['budget']['utilities']
      let transportBudget = testGoals['budget']['transport']
      let foodBudget = testGoals['budget']['food']
      let healthcareBudget = testGoals['budget']['healthcare']
      let personalBudget = testGoals['budget']['personal_care']
      let entertainmentBudget = testGoals['budget']['entertainment']
      let savingsBudget = testGoals['budget']['savings']
      let debtBudget = testGoals['budget']['debt_repayment']

      // set amount spent from goals_test_data
      // should we change the variables for savingsSpent and debtSpent because they don't make sense?

      let housingSpent = testGoals['spent_to_date']['housing']
      let utilitiesSpent = testGoals['spent_to_date']['utilities']
      let transportSpent = testGoals['spent_to_date']['transport']
      let foodSpent = testGoals['spent_to_date']['food']
      let healthcareSpent = testGoals['spent_to_date']['healthcare']
      let personalSpent = testGoals['spent_to_date']['personal_care']
      let entertainmentSpent = testGoals['spent_to_date']['entertainment']
      let savingsSpent = testGoals['spent_to_date']['savings']
      let debtSpent = testGoals['spent_to_date']['debt_repayment']

      //

      let housingLeft = housingBudget - housingSpent
      let utilitiesLeft = utilitiesBudget - utilitiesSpent
      let transportLeft = transportBudget - transportSpent
      let foodLeft = foodBudget - foodSpent
      let healthcareLeft = healthcareBudget - healthcareSpent
      let personalLeft = personalBudget - personalSpent
      let entertainmentLeft = entertainmentBudget - entertainmentSpent
      let savingsLeft = savingsBudget - savingsSpent
      let debtLeft = debtBudget - debtSpent

      return (
        <div id='budgetpage' class='ui container'>

          <h1>My Monthly Budget</h1>
          <PieChart
            width={50}
            height={50}
            radius={25}
	    animate
	    reveal
            data={[
              { title: 'Housing', value: housingBudget, color: '#FFA07A' },
              { title: 'Utilities', value: utilitiesBudget, color: '#F08080' },
              { title: 'Transport', value: transportBudget, color: '#CD5C5C' },
              { title: 'Food', value: foodBudget, color: '#B22222' },
              { title: 'Healthcare', value: healthcareBudget, color: '#FA8072' },
              { title: 'Personal Care', value: personalBudget, color: '#8B0000' },
              { title: 'Entertainment', value: entertainmentBudget, color: '#FF6347' },
              { title: 'Savings', value: savingsBudget, color: '#DB7093' },
              { title: 'Debt Repayment', value: debtBudget, color: '#FF0000' }
            ]}
          />
          <Table color='red' key='red'>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Category</Table.HeaderCell>
                <Table.HeaderCell>Amount Spent</Table.HeaderCell>
                <Table.HeaderCell>Amount Left</Table.HeaderCell>
                <Table.HeaderCell>Budgeted Ammount</Table.HeaderCell>
              </Table.Row>
            </Table.Header>

            <Table.Body>
              <Table.Row>
                <Table.Cell>Housing Budget</Table.Cell>
                <Table.Cell>${housingSpent}</Table.Cell>
                <Table.Cell>${housingLeft}</Table.Cell>
                <Table.Cell>${housingBudget}</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>Utilities</Table.Cell>
                <Table.Cell>${utilitiesSpent}</Table.Cell>
                <Table.Cell>${utilitiesLeft}</Table.Cell>
                <Table.Cell>${utilitiesBudget}</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>Transportation</Table.Cell>
                <Table.Cell>${transportSpent}</Table.Cell>
                <Table.Cell>${transportLeft}</Table.Cell>
                <Table.Cell>${transportBudget}</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>Food</Table.Cell>
                <Table.Cell>${foodSpent}</Table.Cell>
                <Table.Cell>${foodLeft}</Table.Cell>
                <Table.Cell>${foodBudget}</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>Healthcare</Table.Cell>
                <Table.Cell>${healthcareSpent}</Table.Cell>
                <Table.Cell>${healthcareLeft}</Table.Cell>
                <Table.Cell>${healthcareBudget}</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>Personal Care</Table.Cell>
                <Table.Cell>${personalSpent}</Table.Cell>
                <Table.Cell>${personalLeft}</Table.Cell>
                <Table.Cell>${personalBudget}</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>Entertainment</Table.Cell>
                <Table.Cell>${entertainmentSpent}</Table.Cell>
                <Table.Cell>${entertainmentLeft}</Table.Cell>
                <Table.Cell>${entertainmentBudget}</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>Savings</Table.Cell>
                <Table.Cell>${savingsSpent}</Table.Cell>
                <Table.Cell>${savingsLeft}</Table.Cell>
                <Table.Cell>${savingsBudget}</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>Debt Repayment</Table.Cell>
                <Table.Cell>${debtSpent}</Table.Cell>
                <Table.Cell>${debtLeft}</Table.Cell>
                <Table.Cell>${debtBudget}</Table.Cell>
              </Table.Row>
            </Table.Body>
          </Table>

          <br />
          <br />
        </div>
      )
    } else {
      return (
        <Container textAlign='center' style={{ marginTop: '40px' }}>
          <Header as='h1'> YOU HAVE NO GOALS </Header>
        </Container>
      )
    }
  }
}

export default Budget
