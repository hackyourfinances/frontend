import React, { Component } from 'react'
import { Progress } from 'react-sweet-progress'
import 'react-sweet-progress/lib/style.css'

class Learn extends Component {
  constructor (props) {
    super(props)
    this.state = {
      userData: this.props.userData
    }
  }

  render () {
    return (

      <div id='learnpage' class='ui container'>

        <div class='zoom' />

        <h2>Total Progress</h2>

        <Progress type='circle' percent={72} status='active' />
        <h2>My Courses</h2>
        <h4><a href='#basic-budgeting'>Basic Budgeting</a></h4>
        <p><Progress

          percent={12}
          theme={{
            success: {
              symbol: '🏄‍',
              color: 'rgb(223, 105, 180)'
            },
            active: {
              symbol: '😀',
              color: '#fbc630'
            },
            default: {
              symbol: '😱',
              color: 'orange'
            }
          }}
          status='default'
        /></p>
        <h4><a href='#impact-of-interest'>Impact of Interest</a></h4>
        <p><Progress

          percent={82}
          theme={{
            success: {
              symbol: '🏄‍',
              color: 'rgb(223, 105, 180)'
            },
            active: {
              symbol: '😀',
              color: '#fbc630'
            },
            default: {
              symbol: '😱',
              color: '#fbc630'
            }
          }}
          status='success'
        /></p>
        <h4><a href='#savvy-saving'>Savvy Saving</a></h4>
        <p><Progress percent={32}
          theme={{
            success: {
              symbol: '🏄‍',
              color: 'rgb(223, 105, 180)'
            },
            active: {
              symbol: '😀',
              color: '#fbc630'
            },
            default: {
              symbol: '😱',
              color: 'orange'
            }
          }}
          status='default'
        /></p>
        <h4><a href='#scholarhsips'>Scholarships</a></h4>
        <p><Progress percent={98}
          theme={{
            success: {
              symbol: '🏄‍',
              color: 'rgb(223, 105, 180)'
            },
            active: {
              symbol: '😀',
              color: '#fbc630'
            },
            default: {
              symbol: '😱',
              color: '#fbc630'
            }
          }}
          status='success'
        /></p>

        <h4><a href='#home-buying'>Home Buying</a></h4>
        <p><Progress percent={82} theme={{
          success: {
            symbol: '🏄‍',
            color: 'rgb(223, 105, 180)'
          },
          active: {
            symbol: '😀',
            color: '#fbc630'
          },
          default: {
            symbol: '😱',
            color: '#fbc630'
          }
        }}
        status='success' /></p>
        <h4><a href='#debt-repayment'>Debt Repayment</a></h4>
        <p><Progress percent={67}
          theme={{
            success: {
              symbol: '🏄‍',
              color: 'rgb(223, 105, 180)'
            },
            active: {
              symbol: '😀',
              color: '#fbc630'
            },
            default: {
              symbol: '😱',
              color: '#fbc630'
            }
          }}
          status='active'
        /></p>

        <h2 id='basic-budgeting'>Basic Budgeting</h2>
        <h4>Articles</h4>
        <ul>
          <li><a href='https://mycollegeguide.org/blog/2015/03/7-ways-save-money-college-high-school-sophomore/' target='_blank'>7 Ways to Save Money for College as a High School Sophomore</a>
          </li>
          <li><a href='https://www.prospects.ac.uk/applying-for-university/university-life/7-ways-to-save-money-as-a-student' target='_blank'>7 ways to save money as a student</a>
          </li>
          <li><a href='https://www.moneyunder30.com/how-teens-can-save-money' target='_blank'>How To Save Money As A Teen</a>
          </li>
          <li><a href='https://www.listenmoneymatters.com/money-tips-for-teenagers/' target='_blank'>Money Tips For Teenagers: Your Future Self Will Thank You</a>
          </li>
          <li><a href='https://www.kiplinger.com/article/saving/T063-C006-S001-10-financial-commandments-for-your-30s.html' target='_blank'>10 Financial Commandments for Your 30s</a>
          </li>
          <li><a href='https://www.moneycrashers.com/how-to-make-a-budget/' target='_blank'>12 Personal Budgeting Tips for First Timers</a>
          </li>
          <li><a href='https://www.pennypinchinmom.com/how-to-create-a-budget-beginner/' target='_blank'>How to Create a Budget Even If You Suck at Budgeting</a>
          </li>
          <li><a href='https://www.mappingyourfuture.org/money/budget.cfm' target='_blank'>Start Budgeting</a>
          </li>
          <li><a href='https://www.mymoneycoach.ca/budgeting/what-is-a-budget-planning-forecasting' target='_blank'>What Is Budgeting and Why Is It Important</a>
          </li>
          <li><a href='https://www.youneedabudget.com/the-four-rules/' target='_blank'>The Four Rules</a>
          </li>
        </ul>
        <h4>Videos</h4>
        <ul>
          <li><a href='https://www.youtube.com/watch?v=Dhx8tFp6v6c' target='_blank'>How to Do A Monthly Budget</a>
          </li>
          <iframe width='892' height='502' src='https://www.youtube.com/embed/Dhx8tFp6v6c' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen />
          <li><a href='https://www.youtube.com/watch?v=mMALzmZrCcE' target='_blank'>Budgeting for Beginners</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=R1w1vFBhcIw' target='_blank'>11 Things I Cut from My Budget and Don’t Miss At All</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=8F0mH84w6e4' target='_blank'>You Need A Written Budget</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=MbsPcTWDiIQ' target='_blank'>How to Use A Credit Card the Right Way</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=_JhhyHMsAZE' target='_blank'>A Key to Smart Money Management</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=LdVPoOjwkpI' target='_blank'>How to Make a Personal Weekly Budget</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=76IOIfvaWjk' target='_blank'>How to Budget Your Money Like A Boss</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=kwWxjbv9A4U' target='_blank'>42 Ways to Travel More on a Budget</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=tATZ3PhooW0' target='_blank'>How to Budget Your Savings</a>
          </li>
        </ul>
        <h2 id='impact-of-interest'>Impact of Interest</h2>
        <h4>Articles</h4>
        <ul>
          <li><a href='https://www.investopedia.com/articles/investing/112615/why-interest-rates-affect-everyone.asp' target='_blank'>Why Interest Rates Affect Everyone</a>
          </li>
          <li><a href='https://www.nerdwallet.com/blog/investing/investing-in-30s/' target='_blank'>5 Tips for Investing in Your 30s</a>
          </li>
          <li><a href='https://www.wisebread.com/this-is-how-rich-youd-be-if-youd-saved-the-money-you-earned-in-high-school'>This Is How Rich You'd Be If You'd Saved the Money You Earned in High School</a>
          </li>
          <li><a href='https://www.salliemae.com/college-planning/student-loans-and-borrowing'>Types of student loans</a>
          </li>
          <li><a href='https://thecollegeinvestor.com/22105/best-savings-accounts-students/' target='_blank'>THE BEST SAVINGS ACCOUNTS FOR STUDENTS 2019</a>
          </li>
          <li><a href='https://www.education.com/activity/article/understanding-savings-accounts-interest-rates/' target='_blank'>A Guide to Understanding Savings Accounts and Interest Rates</a>
          </li>
          <li><a href='https://lifehacker.com/misunderstood-money-math-why-interest-matters-more-tha-1635258906' target='_blank'>Why Interest Matters More than You Think</a>
          </li>
          <li><a href='https://www.stlouisfed.org/education/economic-lowdown-podcast-series/episode-14-getting-real-about-interest-rates' target='_blank'>Getting Real About Interest Rates</a>
          </li>
          <li><a href='https://www.windgatewealth.com/the-power-of-compound-interest-and-why-it-pays-to-start-saving-now/' target='_blank'>The Power of Compound Interest and Why It Pays to Start Saving Now</a>
          </li>
          <li><a href='https://lendedu.com/blog/how-much-interest-would-you-earn-on-a-million-dollars'>How Much Interest Would You Earn on a Million Dollars?</a>
          </li>

        </ul>
        <h4>Videos</h4>
        <ul>
          <li><a href='https://www.youtube.com/watch?v=EdUEaeBGqgU' target='_blank'>How Does Compound Interest Work?</a>
          </li>
          <iframe width='892' height='502' src='https://www.youtube.com/embed/EdUEaeBGqgU' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen />
          <li><a href='https://www.youtube.com/watch?v=VLnrIaXEQ4c' target='_blank'>Power of Compounding</a>
          </li>
          <li><a href='https://www.khanacademy.org/economics-finance-domain/core-finance/interest-tutorial/compound-interest-tutorial/v/the-rule-of-72-for-compound-interest'>The Rule of 72 for Compound Interest</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=DmtFWhRAFJ8' target='_blank'>Savings Account Bank Interest Calculation</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=2Xllp5ZGMck' target='_blank'>Chasing the Highest Interest Savings Account</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=_iMHKPPGbrY'>Savings Accounts and Interest</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=O3FRc9USz8g'>Strategies for Paying Down Debt</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=THTirjlR180'>Should I Use a Line of Credit to Pay Off Credit Card Debt?</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=_Dfqa8efCIo'>Why Pay Off Debt If I Can Invest at A Higher Interest Rate?</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=XaHleWu6ry4'>Why Can’t I Just Transfer My Debt to A Lower Interest Rate?</a>
          </li>

        </ul>
        <h2 id='savvy-saving'>Savvy Saving</h2>

        <h4>Articles</h4>
        <ul>
          <li><a href='https://www.thesimpledollar.com/little-steps-100-great-tips-for-saving-money-for-those-just-getting-started/' target='_blank'>How to Save Money: 100 Great Tips to Get You Started</a>
          </li>
          <li><a href='http://money.com/money/4555480/savings-self-employed/' target='_blank'>I’m Saving $900 a Month So I Can Start Working for Myself</a>
          </li>
          <li><a href='https://bettermoneyhabits.bankofamerica.com/en/saving-budgeting/how-to-save-money-tips' target='_blank'>How to Save Money Every Day</a>
          </li>
          <li><a href='https://www.goodfinancialcents.com/ways-to-save-money/' target='_blank'>87 Super Easy Ways to Save Money</a>
          </li>
          <li><a href='https://www.bankrate.com/investing/saving-money-or-investing-which-is-more-important-over-time/' target='_blank'>Is Saving Money or Investing More Important Over Time</a>
          </li>
          <li><a href='https://www.daveramsey.com/blog/the-secret-to-saving-money' target='_blank'>The Secret to Saving Money</a>
          </li>
          <li><a href='https://www.discover.com/online-banking/banking-topics/7-ways-to-save-money-on-family-expenses/' target='_blank'>7 Ways to Save Money on Family Expenses</a>
          </li>
          <li><a href='https://www.moneyunder30.com/7-tips-on-saving-for-college-as-a-teen' target='_blank'>7 Tips On Saving For College As A Teen</a>
          </li>
          <li><a href='https://americasaves.org/for-savers/make-a-plan-how-to-save-money/54-ways-to-save-money' target='_blank'>How Much of My Income Should I Save Every Month?</a>
          </li>
          <li><a href='https://www.thesimpledollar.com/little-steps-100-great-tips-for-saving-money-for-those-just-getting-started/' target='_blank'>54 Ways to Save Money</a>
          </li>
        </ul>
        <h4>Videos</h4>
        <ul>
          <li><a href='https://www.youtube.com/watch?v=h0DEZLrsXVQ' target='_blank'>How to Become Rich Simply By Saving Money</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=9RJuVbjYfwY' target='_blank'>3 Forgotten Laws of Saving Money</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=072wplDy7dc' target='_blank'>The Miracle of Compound Returns</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=fhgH0O0eejA' target='_blank'>Savvy Saving Tips for Your Next Supermarket Shop</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=uYtm9-shc2c' target='_blank'>Saving Money Is a Good Strategy for Young People</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=Eqdj5wRaxd8' target='_blank'>Why Saving for Retirement is More Important than Ever</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=SoHgDXLj9hY' target='_blank'>Saving vs. Investing</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=vFW-gsum4hg' target='_blank'>Why Saving Money Will Not Make You Rich… Do This Instead</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=oMwW46V3s5s' target='_blank'>How to Save Money Like a Minimalist</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=IQ6I7-e_RLI' target='_blank'>A Whole New Way to Think About Saving Money</a>
          </li>
        </ul>
        <h2 id='scholarships'>Scholarships</h2>
        <h4>Articles</h4>
        <ul>
          <li><a href='https://www.salliemae.com/college-planning/financial-aid/understand-college-grants/' target='_blank'>Understand grants for college
          </a>
          </li>
          <li><a href='https://www.scholarships.com/financial-aid/college-scholarships/scholarship-information/scholarship-myths/' target='_blank'>Scholarship Myths</a>
          </li>
          <li><a href='https://www.nerdwallet.com/blog/loans/student-loans/private-student-loans/' target='_blank'>Private Student Loans: 6 Best Lenders for May 2019</a>
          </li>
          <li><a href='https://bigfuture.collegeboard.org/pay-for-college/loans/quick-guide-which-college-loans-are-best' target='_blank'>Quick Guide: Which College Loans Are Best?</a>
          </li>
          <li><a href='https://oedb.org/advice/hacking-financial-aid-33-ways-to-get-money-for-college/' target='_blank'>Hacking Financial Aid: 33 Ways to Get Money for College</a>
          </li>
          <li><a href='https://www.state.nj.us/education/aps/cccs/career/resources/mclesson11.pdf' target='_blank'>Interest: The Cost of Borrowing Money</a>
          </li>
          <li><a href='https://studentaid.ed.gov/sa/types/grants-scholarships/finding-scholarships' target='_blank'>Finding and Applying for Scholarships</a>
          </li>
          <li><a href='https://www.affordablecollegesonline.org/womens-guide-paying-for-college/' target='_blank'>Scholarships for Women</a>
          </li>
          <li><a href='https://www.consumerreports.org/paying-for-college/tips-for-scoring-college-scholarships/' target='_blank'>5 Tips for Scoring College Scholarships</a>
          </li>
          <li><a href='https://bigfuture.collegeboard.org/pay-for-college/grants-and-scholarships/where-to-find-college-scholarships' target='_blank'>5 Where to Find College Scholarships</a>
          </li>

        </ul>
        <h4>Videos</h4>
        <ul>
          <li><a href='https://bigfuture.collegeboard.org/pay-for-college/grants-and-scholarships/where-to-find-college-scholarships' target='_blank'>Where to Find College Scholarships</a>
          </li>
          <iframe width='892' height='502' src='https://www.youtube.com/embed/42-w-tMyl3Y' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen />
          <li><a href='https://www.salliemae.com/college-planning/financial-aid/understand-college-grants/' target='_blank'>Understand grants for college
          </a>
          </li>
          <li><a href='https://www.scholarships.com/financial-aid/college-scholarships/scholarship-information/scholarship-myths/' target='_blank'>Scholarship Myths</a>
          </li>
          <li><a href='https://www.nerdwallet.com/blog/loans/student-loans/private-student-loans/' target='_blank'>Private Student Loans: 6 Best Lenders for May 2019</a>
          </li>
          <li><a href='https://bigfuture.collegeboard.org/pay-for-college/loans/quick-guide-which-college-loans-are-best' target='_blank'>Quick Guide: Which College Loans Are Best?</a>
          </li>
          <li><a href='https://oedb.org/advice/hacking-financial-aid-33-ways-to-get-money-for-college/' target='_blank'>Hacking Financial Aid: 33 Ways to Get Money for College</a>
          </li>
          <li><a href='https://www.state.nj.us/education/aps/cccs/career/resources/mclesson11.pdf' target='_blank'>Interest: The Cost of Borrowing Money</a>
          </li>
          <li><a href='https://studentaid.ed.gov/sa/types/grants-scholarships/finding-scholarships' target='_blank'>Finding and Applying for Scholarships</a>
          </li>
          <li><a href='https://www.affordablecollegesonline.org/womens-guide-paying-for-college/' target='_blank'>Scholarships for Women</a>
          </li>
          <li><a href='https://www.consumerreports.org/paying-for-college/tips-for-scoring-college-scholarships/' target='_blank'>5 Tips for Scoring College Scholarships</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=42-w-tMyl3Y' target='_blank'>How to Get Scholarships for College! FULL RIDES, Local Scholarships, Application Tips!</a>
          </li>

        </ul>

        <h2 id='debt-repayment'>Debt Repayment</h2>
        <h4>Articles</h4>
        <ul>
          <li><a href='https://www.moneyunder30.com/how-student-loans-work' target='_blank'>How Student Loans Work</a>
          </li>
          <li><a href='https://www.magnifymoney.com/blog/college-students-and-recent-grads/college-students-face-high-interest-rates-on-student-credit-cards/' target='_blank'>Study: College Students Face High Interest Rates on Student Credit Cards</a>
          </li>
          <li><a href='https://www.simpletuition.com/student-loans/private/bad-credit/' target='_blank'>Student Loans for Bad Credit</a>
          </li>
          <li><a href='https://smartasset.com/student-loans/student-loan-calculator' target='_blank'>Student Loan Calculator</a>
          </li>
          <li><a href='https://workethic.org/why-its-important-for-young-people-to-have-part-time-jobs/' target='_blank'>Why It’s Important for Young People to Have Part-Time Jobs</a>
          </li>
          <li><a href='https://www.bustle.com/p/ive-paid-18000-to-a-24000-student-loan-i-still-owe-24000-9000788'>I've Paid $18,000 To A $24,000 Student Loan, & I Still Owe $24,000</a>
          </li>
          <li><a href='https://www.cnbc.com/2017/07/11/habits-to-develop-in-your-20s-to-get-out-of-debt-by-30.html'>5 things to do in your 20s to get out of debt by 30</a>
          </li>
          <li><a href='https://thecollegeinvestor.com/19745/manage-credit-card-debt-20s/'>HOW TO MANAGE YOUR CREDIT CARD DEBT IN YOUR 20S</a>
          </li>
          <li><a href='https://www.daytondailynews.com/news/communities-luring-young-professionals-with-extra-money-incentives/xtb1L3cLa7EoQSK9PmGIzN/'>See what area cities, businesses are doing to lure young professionals</a>
          </li>
          <li><a href='https://www.debt.org/students/debt/'>Managing Student Loan Debt</a>
          </li>
        </ul>
        <h4>Videos</h4>
        <ul>

          <li><a href='https://www.youtube.com/watch?v=TWHV-nRuuoQ'>The Two Easiest Ways to Pay Down Your Debt</a>
          </li>
          <iframe width='892' height='502' src='https://www.youtube.com/embed/TWHV-nRuuoQ' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen />
          <li><a href='https://www.khanacademy.org/college-careers-more/personal-finance/pf-interest-and-debt/debt-repayment/v/high-rate-vs-snowball-method'>High Rate Versus Snowball Method</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=qdkSwU1QhHc'>Creating A Debt Repayment Plan</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=GS8x_q_Aa_4'>The Dos and Don’ts of Debt Repayment</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=r7JcHz6ucyI'>I Pay $900 A Month In Student Loans</a>
          </li>
          <li><a href='https://collegeaffordability.com/resources/student-loan-repayment-videos/'>Student Loan Repayment Videos</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=beMqpOSPp9I'>How I Paid Off $97,000 in Student Loans in 4 Years</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=N07KfQd0m5s'>The Student Loan Debt Crisis Is Worse Than You Think</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=Abz9qgi9FKg'>I Owe $430,000 of Student Loans!</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=P7Nvg_0mXCM'>Tackling College Debt</a>
          </li>
        </ul>

        <h2 id='home-buying'>Home Buying</h2>
        <h4>Articles</h4>
        <ul>
          <li><a href='https://www.moneyunder30.com/first-time-home-buying-guide' target='_blank'>First-Time Home Buying Guide</a></li>
          <li><a href='https://www.bankrate.com/finance/real-estate/should-you-buy-an-old-or-new-home-1.aspx'>Should you buy an old or new home?</a>
          </li>
          <li><a href='https://www.debt.org/real-estate/buying-house/'>Buying and Owning a House</a>
          </li>
          <li><a href='https://www.marketplace.org/2018/06/01/economy/rules-home-buying'>The basic rules of buying a house</a>
          </li>
          <li><a href='https://www.incharge.org/housing/homebuyer-education/homeownership-guide/advantages-and-disadvantages-of-owning-a-home/'>Advantages and Disadvantages of Owning a Home</a>
          </li>
          <li><a href='https://studentloanhero.com/featured/should-i-buy-a-house/'>5 Signs You’re Not Ready to Buy a House Yet</a>
          </li>
          <li><a href='https://twocents.lifehacker.com/should-i-buy-a-home-or-just-keep-renting-1699277766'>Should I Buy a Home or Just Keep Renting?</a>
          </li>
          <li><a href='https://www.usatoday.com/story/money/personalfinance/real-estate/2018/07/31/best-age-to-buy-first-home-how-to-pull-it-off/37032849/'>Most Americans think 28 is best age to buy first home. Here's how to pull that off</a>
          </li>
          <li><a href='https://www.trulia.com/blog/home-buying-tips-for-every-age/'>Tips To Buy A Home In Your 20s, 30s, And 40s</a>
          </li>
          <li><a href='https://themortgagereports.com/26032/young-homebuyers-how-young-is-too-young-to-buy-a-house'>Young Homebuyers: What’s The Right Age To Buy A House?</a>
          </li>
        </ul>
        <h4>Videos</h4>
        <ul>
          <li><a href='https://www.youtube.com/watch?v=1xwbodhLDIg'>7 Steps to Buying a House</a>
          </li>
          <iframe width='892' height='502' src='https://www.youtube.com/embed/1xwbodhLDIg' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen />
          <li><a href='https://www.youtube.com/watch?v=o0J0Ygn20NI'>Home Buying Tips: How to Buy A House</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=xGE4a9IMZwM'>First Time Home Buyers Guide - Tips and Advice</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=11oithCm_CM'>The Home Buying Process Explained in Two Minutes</a>
          </li>
          <li><a href='https://www.khanacademy.org/economics-finance-domain/core-finance/housing/renting-v-buying/v/renting-vs-buying-a-home'>Is Buying A Home Always Better?</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=ChSpkZ0jVxc'>Should You Buy A House?</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=TbgvaypNdB4'>Buying A House: 10 Things You Need to Do</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=6LOSWe_a560'>Is It Time to Buy A House?</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=NGX4oJ0tjo0'>Flipping Houses vs Rental Property Investing: Which Is Best?</a>
          </li>
          <li><a href='https://www.youtube.com/watch?v=UH-KozlA1FA'>How Much Do House Flippers Make Per Year?</a>
          </li>
        </ul>

      </div>
    )
  }
}

export default Learn
