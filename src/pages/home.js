import React, { Component } from 'react'
import { Container, Form, Button, Input, Select, Header } from 'semantic-ui-react'

const options = [
  { key: 'hs', text: 'High School Student', value: '1' },
  { key: 'cs', text: 'College Student', value: '2' },
  { key: 'yp', text: 'Young Professional', value: '3' },
  { key: 'mp', text: 'Mid-Level Professional', value: '4' }]

class Assets extends Component {
  constructor (props) {
    super(props)

    this.state = {
      service: this.props.service,
      category: '',
      monthlyIncome: 0
    }

    this.updateCategory = this.updateCategory.bind(this)
    this.updateMonthlyIncome = this.updateMonthlyIncome.bind(this)
  }

  updateCategory (e, { value }) {
    this.setState({ category: value })
  }

  updateMonthlyIncome (e, { value }) {
    this.setState({ monthlyIncome: value })
  }

  handleSubmit () {
    console.log('' + this.state.category + ' ' + this.state.monthlyIncome)
    this.state.service.putAsset(this.category, this.monthlyIncome).then(() => {
      window.alert('Thank You for Telling Us About You!')
    })
  }

  render () {
    return (
      <Container>
        <Header as='h2'>
          Tell Us About You!
        </Header>
        <Form>
          <Form.Field
            control={Input}
            label='Monthly Income'
            placeholder='2000'
            onChange={this.updateMonthlyIncome}
          />
          <Header as='h3'>
            You are a...
          </Header>
          <Form.Group>
            <Form.Field
              control={Select}
              options={options}
              onChange={this.updateCategory}
            />
          </Form.Group>
          <Button primary onClick={this.handleSubmit.bind(this)}>
            Submit
          </Button>
        </Form>
      </Container>
    )
  }
}

export default Assets
