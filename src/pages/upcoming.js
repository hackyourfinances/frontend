import React, { Component } from 'react'
import { Container, Form, Button, Input, Select, Header } from 'semantic-ui-react'

const options = [
  { key: 'hs', text: 'High School Student', value: '1' },
  { key: 'cs', text: 'College Student', value: '2' },
  { key: 'yp', text: 'Young Professional', value: '3' },
  { key: 'mp', text: 'Mid-Level Professional', value: '4' }]

class Upcoming extends Component {
  constructor (props) {
    super(props)

    this.state = {
      service: this.props.service,
      category: '',
      monthlyIncome: 0
    }

    this.updateCategory = this.updateCategory.bind(this)
    this.updateMonthlyIncome = this.updateMonthlyIncome.bind(this)
  }

  updateCategory (e, { value }) {
    this.setState({ category: value })
  }

  updateMonthlyIncome (e, { value }) {
    this.setState({ monthlyIncome: value })
  }

  handleSubmit () {
    console.log('' + this.state.category + ' ' + this.state.monthlyIncome)
    this.state.service.putAsset(this.category, this.monthlyIncome).then(() => {
      window.alert('Thank You for Telling Us About You!')
    })
  }

  render () {
    return (
      <Container>
        <Header as='h2'>
          Upcoming Features
        </Header>
        <body>
          <p />
          <ul>
            <ul><h4>Tailor personalization to demographics</h4>
              <li>Zip code</li>
              <li>Personalized goals</li>
              <li>Career stage</li>
              <li>Financial literacy Courses</li>
            </ul>
            <br />
            <ul><h4>Use machine learning and AI to predict future needs</h4>
              <li>Base on user data and activities</li>
              <li>Take the user through life stages</li>

            </ul>
            <br />
            <ul><h4>User-created goals</h4>
              <li>Make your own choices about the goals you're working toward</li>
              <li>Measure your own progress</li>

            </ul>
            <br />
            <ul><h4>Learning page</h4>
              <li>Tracking percentage completion of each course</li>
              <li>Personalized reccomendations for further learning</li>
              <li>Badges for completion of levels!</li>
              <li>Quizzes for articles</li>
              <li>Add color coding to show which courses need more attention</li>
            </ul>
            <br />
            <ul><h4>Integration with bank accounts</h4>
              <li>Eliminate manually tracking spending</li>
              <li>AI insights into spending trends to help you avoid overspending and/or underreporting</li>
            </ul>
            <br />
            <ul><h4>Integration with FAFSA account</h4>
              <li>Dynamically update loan status and payments</li>
              <li>Central location for contact information</li>
              <li>Easy online payment integration</li>
            </ul>

          </ul>

        </body>
        <h3>FAQ</h3>
        <h5>Which Features are Published?</h5>
        <p>At its own discretion, Hack Your Finances publishes features and bug fixes based on the number of customer reports, the severity of the issue, and the availability of a workaround. If an issue you are encountering is not listed, it may not have fit the criteria for publishing on the upcoming features site. Not all known bugs are published: often bugs are resolved quickly or do not affect customers.</p>
        <h5>Are these features guaranteed?</h5>
        <p>Any unreleased services, features, statuses, or dates referenced in this or other public statements are not currently available and may not be delivered on time or at all. Customers who use our services should make their usage decisions based upon features that are currently available.</p>
        <h5>How can I report an issue?</h5>
        <p>Hack Your Finances appreciates all issue reports. Please include them in the form below as well as information about how to reproduce the issue.</p>
        <br />
        <Form>
          <Form.Field
            control={Input}
            label='Request a Feature'
            placeholder='What would you like us to add?'
            onChange={this.updateMonthlyIncome}
          />

          <Button primary>
            Submit
          </Button>
        </Form>
        <br /><br /><br />

      </Container>
    )
  }
}

export default Upcoming
