const testGoals =
{
  name: 'Jimmy Sticks',
  category: 'College Student',
  monthly_income: 89120391,
  goal_name: 'Pay off the Loansharks',
  goal_desc: 'I need to pay off the loansharks before Friday',
  goal_target: 3000000,
  courses: [
    {
      course_name: 'Basic Budgeting',
      cource_score: 15
    },
    {
      course_name: 'Impact of Interest',
      cource_score: 74
    },
    {
      course_name: 'Savvy Savings',
      cource_score: 11
    },
    {
      course_name: 'Debt Repayment',
      cource_score: 32
    }
  ],
  budget: {
	  housing: 1200,
	  utilities: 200,
	  transport: 75,
	  food: 190,
	  healthcare: 90,
	  personal_care: 125,
	  entertainment: 100,
	  savings: 200,
	  debt_repayment: 290
  },
  spent_to_date: {
	  housing: 1200,
	  utilities: 190,
	  transport: 33,
	  food: 100,
	  healthcare: 200,
	  personal_care: 225,
	  entertainment: 90,
	  savings: 200,
	  debt_repayment: 210

  }
}

export { testGoals }
