import React, { Component } from 'react'
import { Menu, Header } from 'semantic-ui-react'
import { NavLink } from 'react-router-dom'

class Navigation extends Component {
  constructor (props) {
    super(props)

    this.state = {
      activeItem: 'none',
      service: this.props.service,
      userData: this.props.userData
    }
  }

  handleClick (e, { name }) {
    this.setState({ activeItem: name })
  }

  render () {
    return (
      <Menu color='red'>
        <Menu.Item
          name=''
          as={NavLink} exact to='/'
          active={this.activeItem === ''}
          onClick={this.handleClick}
        >
          Home
        </Menu.Item>
        <Menu.Item
          name='goals'
          as={NavLink} exact to='/goals'
          active={this.activeItem === 'goals'}
          onClick={this.handleClick}
        >
          Goals
        </Menu.Item>

        <Menu.Item
          name='budget'
          as={NavLink} exact to='/budget'
          active={this.activeItem === 'budget'}
          onClick={this.handleClick}
        >
          Budget
        </Menu.Item>
        <Menu.Item
          name='learn'
          as={NavLink} exact to='/learn'
          active={this.activeItem === 'learn'}
          onClick={this.handleClick}
        >
          Learn
        </Menu.Item>
        <Menu.Item
          name='upcoming'
          as={NavLink} exact to='/upcoming'
          active={this.activeItem === 'upcoming'}
          onClick={this.handleClick}
        >
          Upcoming
        </Menu.Item>
	<Menu.Item name='Mary Thompson'>
          Mary Thompson
	</Menu.Item>
      </Menu>
    )
  }
}

export default Navigation
